package org.springframework.samples.petclinic.rest;

import io.restassured.specification.RequestSpecification;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.samples.petclinic.DatasourceConfig;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
@SpringBootTest(classes = { DatasourceConfig.class, })
public class RestApiBaseTest {

	@Resource
	public DatasourceConfig datasourceConfig;

	public static Connection connection;

	@Before
	public void setup() throws SQLException {
		connection = DriverManager.getConnection(datasourceConfig.getUrl(), datasourceConfig.getUsername(),
				datasourceConfig.getPassword());
	}

	@After
	public void tearDown() throws SQLException {
		connection.close();
	}

}
