package org.springframework.samples.petclinic.rest;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

class VetControllerTest extends RestApiBaseTest {

	@Test
	void showResourcesVetList() {
		given().when().get("/vets").then().assertThat().statusCode(200).body("vetList[0].id", is(1));
	}

}
