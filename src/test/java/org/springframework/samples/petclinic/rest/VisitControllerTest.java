package org.springframework.samples.petclinic.rest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class VisitControllerTest extends RestApiBaseTest {

	int petID;

	int ownerID;

	int VisitID;

	String petName = RandomStringUtils.randomAlphabetic(3);

	String ownerFName = RandomStringUtils.randomAlphabetic(7);

	String ownerLName = RandomStringUtils.randomAlphabetic(7);

	String ownerAddress = RandomStringUtils.randomAlphabetic(10);

	String ownersPhone = "" + RandomUtils.nextInt(600000000, 699999999);

	LocalDate petBirthDate = LocalDate.now().minusDays(1);

	LocalDate visitDate = LocalDate.now();

	@Before
	public void prepareTestData() throws SQLException {
		Statement maxOwnerID = connection.createStatement();
		ResultSet maxOwnerResult = maxOwnerID.executeQuery("select max(ID) from owners");
		maxOwnerResult.next();
		ownerID = maxOwnerResult.getInt(1) + 1;

		Statement maxPetID = connection.createStatement();
		ResultSet maxPetResult = maxPetID.executeQuery("select max(ID) from pets");
		maxPetResult.next();
		petID = maxPetResult.getInt(1) + 1;

		Statement maxVisitID = connection.createStatement();
		ResultSet maxVisitResult = maxVisitID.executeQuery("select max(ID) from pets");
		maxVisitResult.next();
		VisitID = maxVisitResult.getInt(1) + 1;

		PreparedStatement owner = connection.prepareStatement(
				"INSERT INTO OWNERS(id, first_name, last_name, address, city,telephone) VALUES(?,?,?,?,?,?)");
		owner.setInt(1, ownerID);
		owner.setString(2, ownerFName);
		owner.setString(3, ownerLName);
		owner.setString(4, ownerAddress);
		owner.setString(5, "TO");
		owner.setString(6, ownersPhone);
		owner.executeUpdate();

		PreparedStatement pet = connection
				.prepareStatement("INSERT INTO pets(id, name, owner_id, birth_date) VALUES(?,?,?,?)");
		pet.setInt(1, petID);
		pet.setString(2, petName);
		pet.setInt(3, ownerID);
		pet.setObject(4, petBirthDate);
		pet.executeUpdate();

		PreparedStatement visit = connection
				.prepareStatement("INSERT INTO visits(id, pet_id, visit_date, description) VALUES(?,?,?,?)");
		visit.setInt(1, VisitID);
		visit.setInt(2, petID);
		visit.setObject(3, visitDate);
		visit.setString(4, "Test");
		visit.executeUpdate();
	}

	@After
	public void clearTestsData() throws SQLException {
		Statement checkPets = connection.createStatement();
		String sqlPets = "SELECT * FROM pets where id >= " + petID;
		ResultSet petsResult = checkPets.executeQuery(sqlPets);
		if (petsResult.next()) {
			PreparedStatement delete = connection.prepareStatement("DELETE FROM pets WHERE ID >= ?");
			delete.setInt(1, petID);
			delete.executeUpdate();
		}

		Statement checkOwners = connection.createStatement();
		String sqlOwners = "SELECT * FROM owners where id >= " + ownerID;
		ResultSet ownersResult = checkOwners.executeQuery(sqlOwners);
		if (ownersResult.next()) {
			PreparedStatement delete = connection.prepareStatement("DELETE FROM owners WHERE ID >= ?");
			delete.setInt(1, ownerID);
			delete.executeUpdate();
		}

		Statement checkVisits = connection.createStatement();
		String sqlVisit = "SELECT * FROM owners where id >= " + (VisitID);
		ResultSet visitsResult = checkVisits.executeQuery(sqlVisit);
		if (visitsResult.next()) {
			PreparedStatement delete = connection.prepareStatement("DELETE FROM owners WHERE ID >= ?");
			delete.setInt(1, VisitID);
			delete.executeUpdate();
		}
	}

	@Disabled
	@Test
	@DisplayName("initNewVisitForm")
	/**
	 * Данный метод должен возвращать список визитов питомца в клинику. Однако в
	 * дополнение он возвращает несуществующий визит от текущей даты. Вероятно из примера.
	 */
	public void getAllVisitsByPetID() {
		when().get("owners/" + ownerID + "/pets/" + petID + "/visits").then().statusCode(200).body("size()", is(1));
	}

	@Disabled
	@Test
	@DisplayName("processNewVisitForm")
	/**
	 * Метод должен постировать новый визит питомца, однако на текущий момент это не
	 * происходит. Исходя из кода сделаны выводы, что метод не доработан
	 */
	public void postNewVisitAndGetFullInfo() {
		LocalDate testDate = LocalDate.now().minusDays(1);
		given().contentType("application/json")
				.body("{\n" + "  \"date\": \"" + testDate + "\",\n" + "  \"description\": \"test Visit\"\n" + "}")
				.when().post("owners/" + ownerID + "/pets/" + petID + "/visits").then().statusCode(201)
				.body("pets[0].visits[0].date", is(testDate), "pets[0].visits[0].date", not(null));
	}

}
